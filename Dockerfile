FROM php:apache

LABEL disclaimer="This image is for testing, never use it in a production environment"

RUN apt update && apt install -y git
RUN git clone https://github.com/JohnTroony/php-webshells /tmp/php-webshells

RUN mkdir /var/www/geronimo
RUN cp /tmp/php-webshells/Collection/b374k-mini-shell.php /var/www/geronimo/index.php

COPY geronimo.conf /etc/apache2/sites-enabled/
